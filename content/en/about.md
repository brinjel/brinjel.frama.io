---
title: "About"
description: "Free and open source crop-planning for market gardeners"
---

Hi there!

I'm André Hoarau, the creator of Brinjel and [Qrop](https://qrop.frama.io).
