---
date: 2024-08-24
title: "Major update of Brinjel"
slug: "2024-08-24-major-update-of-brinjel"
tags: Maintenance
summary: Maintenance is scheduled for Sunday, August 25, 2024 from 8:00 a.m. (UTC+2) in order to carry out a major update to Brinjel.
---

This update will improve performance and enable multiple users per farm.

The outage should last no more than 2 hours. For more information, please checkout the [status page](http://status.brinjel.com).

_Maintenance is now complete._
