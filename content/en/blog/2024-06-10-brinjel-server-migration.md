---
date: 2024-06-10
title: "Brinjel server migration"
slug: "2024-06-10-brinjel-server-migration"
tags: Maintenance
summary: Maintenance is scheduled for Tuesday, June 11, 10 a.m., to migrate Brinjel to a new server.
---

The outage should last no more than 2 hours. For more information, please checkout the [status page](http://status.brinjel.com).

_Maintenance is now complete._
