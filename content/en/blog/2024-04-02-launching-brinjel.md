---
date: 2024-04-02
title: "Launching Brinjel"
slug: "launching-brinjel"
tags: News
summary: After more than a year of development, I’m proud to announce the launch of Brinjel, the open source crop planning and recordkeeping tool for market gardening.
---

![Crop Plan in Brinjel](/img/brinjel-release-fr.png)

Try it free for one month!

[Try Brinjel free](https://app.brinjel.com/register)

Brinjel is the successor of Qrop and has most of its features, while being
accessible online. If you already use Qrop, you can import all your data into
Brinjel with just a few clicks. Over the next few weeks, I’ll be adding new
features to make it even easier to work with your team mates. I’ll also publish
detailed documentation and tutorials soon.

You can request features and let me know about any problems/bugs you’ve
encountered.

For those of you who speak Spanish: I’ve noticed your growing interest in
Brinjel and I’m thrilled to announce that I’ll be working on a translation for
you!

Thank you for your interest in a open source crop planning tool made by a
former market gardener.
