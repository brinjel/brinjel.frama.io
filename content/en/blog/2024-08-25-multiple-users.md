---
date: 2024-08-25
title: "Use Brinjel with your team mates"
slug: "2024-08-25-use-brinjel-with-your-team-mates"
tags: News
summary: It's now possible to create several accounts per farm, with different roles and permissions.
---

## Access to farms

When you log in to your Brinjel account, you will now see a list of farms to
which you have access. If you have invitations for other farms, you'll also see
them listed.

![List of farms and invitations to other farms](/img/farm-list-fr.png)

By clicking on the **Accept** button, the farm becomes available:

![List of farms with accepted invitation](/img/farm-list-invitation-accepted-fr.png)

Simply click on the farm you wish to access and the crop plan will appear.

Page addresses now include the farm name. It is thus possible to consult
several farms at the same time by opening several tabs or windows.

![Simultaneous viewing of several farms](/img/multiple-farms-fr.png)

## Invite guests to your farm

To invite someone to your farm, first [subscribe](https://docs.brinjel.com/en/trial-to-paid/). Next, go to **Parameters** then **Team members**. By clicking on the “Add invitation” button, a form appears. Enter the guest's e-mail address and the role you wish to assign.

![Inviting a guest](/img/inviting-member-fr.png)

The invited person will then receive an invitation e-mail. If they already have
a Brinjel account, they'll simply have to log in to their account and accept the
invitation from [the farm list](https://app.brinjel.com/farms). Otherwise, they
can create an account using the link in the invitation email.
