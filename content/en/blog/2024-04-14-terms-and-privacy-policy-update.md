---
date: 2024-04-14
title: "Terms of Service update"
tags: Company
summary: Minor update of our Terms of Service and Privacy policy.
---

As part of the implementation of our billing system, we had to update our [Terms of Service](/en/policies/terms) and our [Privacy Policy](/en/policies/privacy), in particular to specify payment and plan change procedures.
