---
title: What's New
subtitle: We're constantly improving Brinjel.
description: Here are some of the features and improvements that we’ve made to Brinjel since it first launched.
_build:
  render: true
cascade:
  _build:
    render: false
    list: true
---
