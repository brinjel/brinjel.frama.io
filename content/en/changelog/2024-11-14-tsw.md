---
title: Thousand seed weight
date: 2024-11-14T17:10:00+0200
tags: News
---

![Thousand seed weight field in planting form](/img/changelog-thousand-seed-weight-en.png)

Your favorite seed provider gives seed weight in TSW (Thousand Seed Weight)
instead of seeds per gram? No worries, we've added a dedicated field to the
planting form.

You can fill one of the fields, the other will be converted automatically.
