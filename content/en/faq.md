---
title: "Frequently asked questions"
---

## Migrating to Brinjel

### Will it be easy to migrate from Qrop?

_Yes_, as easy as importing your Qrop database (`.sqlite` or `.db`)! You'll just
have to upload it to Brinjel, and you'll be ready to plan. Some parts of the
user interface is going to change, but we tried to keep the Qrop user experience
as much as possible to make the migration easy.

### Will it be easy to import from Excel, Calc or CSV file?

We're going to improve the CSV import feature, which had several bugs in Qrop.

### Will Brinjel still be free and open source software?

**Yes!** We firmly believe in free software values and trust the Qrop community.
We believe it is possible to make a living with free software which respects
your data and listens to its users. Qrop's desktop version was
[GPL](https://www.gnu.org/licenses/licenses.html#AGPL)-licensed. Brinjel will
use the [AGPL](https://www.gnu.org/licenses/licenses.html#AGPL) license, which
is even more protecting, since it has _"an additional term to allow users who
interact with the licensed software over a network to receive the source for
that program"_.

### Will it still be possible to user Qrop's desktop version?

Yes, but we won't do any maintenance or bug fixing. Our all working time will be
dedicated to Brinjel's development. The source code of Qrop will still be free,
but [the Framagit repository](https://framagit.org/ah/qrop) and all relevant
issues will be transferred to to Brinjel's repository.

### Will it be possible to use Brinjel offline?

No, it won't be possible to use Brinjel offline, unless you self-host it on your
computer. It seems to us that most Qrop users have access to reliable internet
connections, and we prefer to dedicate our time to team features. It it not
impossible that one day we will add an offline feature, but this is not a top
priority for us.

## Data

### Will my data be safe?

We will make daily backups of your data. Moreover, the data stored on our
servers will be isolated by farm. It will no be possible for another Brinjel
user to access your data if you didn't give the rights to her. You will be able
to download your whole farm data stored on our servers. No farm data will be
transmitted to third parties. Your data belongs to you!

## Subscription

### Will I have to pay to use Brinjel?

Since Brinjel is free and open source software, every one with the necessary
knowledge will be able to self-host it and use all its features. However, those
who want maintenance and support will be able to pay for
[brinjel.com](https://brinjel.com) service. This will ensure a stable revenue
for Brinjel's developer, who will be able to work full-time on the app's
development, maintenance and infrastructure.

### How much will it cost?

We know about the economical situation of many farmers. We've been through all
of this during 6 years. That's why the subscription will cost _5 €/5 $ per
month_ or _54 €/54 $_ per year. Sure, if you have a better financial situation
or have a bigger farm, you will be able to pay more! We won't track your farm
size for these criteria. If needed, we may put some restrictions on your data
size (eg. storing many photos may have a cost). You trust us, we trust you.

## Training

### Are there any training sessions planned?

For now, we don't plan training sessions or workshops in English.
