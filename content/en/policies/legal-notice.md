---
title: "Legal Notice"
---

## Publisher

E.I John Hoarau\
36 chemin Désiré\
97429 Petite-Île\
France

Siret n° 880 817 937 00027

[Contact](/en/contact)

## Hosting company

Hetzner Online GmbH\
Industriestr. 25\
91710 Gunzenhausen\
Germany

Tel.: +49 (0)9831 505-0


