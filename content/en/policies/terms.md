---
title: "Terms of Service"
---

_Last update: 14 April 2023._

## Definitions

When we say “Services”, we mean our websites `brinjel.com` and `app.brinjel.com`.

When we say "Host", "we", "us" and "our", we are referring to the publisher of the site defined in the [legal notice](/en/policies/legal-notice).

When we say “User”, “You” or “your”, we are referring to any individual or
legal entity using our services.

When we say "Terms of Service", we are referring to this document, which
defines the rules and rights of the User and the Host.

## Preamble

We may update these Terms of Service in the future. Whenever we make a
significant change to our policies, we will also announce them on [our blog](/en/blog).

When you use our service, now or in the future, you are agreeing to the latest
Terms of Service. That’s true for any of our existing and future products and
all features that we add to our service over time.

If you do not agree to these Terms of Service, do not use this service.

Note: The following Terms of Service apply to Brinjel Cloud only and not to
Brinjel Self-Hosted which would be hosted on your own servers and therefore the
Terms of Service isn’t needed.

## Account Terms

1. You are responsible for maintaining the security of your account and
   password and for ensuring that any of your users do the same. The Host
   cannot and will not be liable for any loss or damage from your failure to
   comply with this security obligation.
1. You are responsible for all content posted to and activity that occurs under
   your account, including content posted by and activity of any users in your
   account.
1. You may not use the Services for any illegal or unauthorized purpose. You
   must not contravene the laws of your country by using our services.
1. Accounts can only be created and used by humans. Accounts created by robots
   or other automated methods may be deleted without warning.
1. You may not sell, trade, resell, or exploit for any unauthorized commercial
   purpose any account of the Service.

## Payment, refunds, upgrading and downgrading terms

1. For our free trial, we explain the length of trial when you sign up. We do
   not ask you for your credit card and --- just like for customers who pay for
   our Services --- we do not sell your data. After the trial period, you need to
   pay in advance to keep using the service. If you do not pay, we will freeze
   your account and it will be inaccessible until you make payment. If your
   account has been frozen for 60 days, we will queue it up for auto-cancellation.
1. If you are upgrading from a free trial to a paid plan, we will charge you
   immediately and your billing cycle starts on the day of upgrade.
1. Paying customers are billed automatically via credit card or PayPal depending on their preference.
1. Our order and payment processes are conducted by our online reseller &
   Merchant of Record, Paddle.com, who also handles order-related inquiries and
   returns. All fees are inclusive of all taxes, levies, or duties imposed by
   taxing authorities. Paddle will collect those taxes on behalf of taxing
   authority and remit those taxes to taxing authorities. See
   [Paddle’s Terms of Use](https://www.paddle.com/legal/terms) for details.
1. No payment data (e.g. credit card number) is handled directly by the Host.
   Payment is considered validated when Paddle informs the Host
   (e.g., via an automatic return message). If the service provider returns a
   message indicating that the payment has failed, the user cannot assert any
   rights against the host.
1. You can upgrade in plan level at any time within your account
   settings. Downgrading your plan may cause the loss of features or capacity
   of your account. The Host does not accept any liability for such
   loss.
1. You can upgrade at any time within your account settings. In this case, you will immediately benefit from the higher plan and you will be billed pro rata on your next invoice. You can upgrade from a monthly plan to an equal or higher annual plan at any time. In this case, you will be billed immediately on a pro rata basis and a new payment cycle will begin on the day of the payment. For technical reasons, any other change to your plan (downgrading to a lower plan or from an annual to a monthly payment) cannot be carried out automatically.
1. Fees paid hereunder are non-refundable.

## Cancellation and Termination

1. You are solely responsible for properly canceling your account. To cancel
   your account, we provide a simple no-questions-asked cancellation link in
   your account's settings. An email or phone request to cancel your
   account is not automatically considered cancellation. If you need help
   canceling your account, you can always [contact us](/en/contact).
1. If you cancel the service before the end of your current paid up period
   (monthly or yearly), your cancellation will take effect at the end of the
   current billing cycle, and you will not be charged again.
1. We reserve the right to suspend or terminate your account and refuse any and
   all current or future use of the service for any reason at any time. Such
   termination of the service will result in the deactivation or deletion of your
   account or your access to your account and site stats.
1. The Host reserves the right to refuse service to anyone for any reason at any time. We
   have this clause because statistically speaking, out of the thousands of sites
   on our service, there may be at least one doing something nefarious.
1. The web host also reserves the right to terminate your account if your
   e-mail address has not been validated within one month.
1. Verbal, physical, written or other abuse (including threats of abuse or
   retribution) of a Host employee, officer or client will result in immediate
   account termination.

## Modifications to the service and prices

1. We reserve the right at any time and from time to time to modify or
   discontinue, temporarily or permanently, any part of the service with or
   without notice.
1. Sometimes we change the pricing structure for our products. When we do that,
   we tend to exempt existing customers from those changes. However, we may
   choose to change the prices for existing customers. If we do so, we will give
   at least 30 days notice and will notify you via the email address on record. We
   may also post a notice about changes on [our blog](/en/blog) or the affected services
   themselves.
1. The Host shall not be liable to you or to any third-party for any
   modification, price change, suspension or discontinuance of the service.

## Content ownership, copyright and trademark

1. You are solely responsible for any content and other material that you
   submit, publish, transmit, email, or display on, through, or with the
   service.
1. We claim no intellectual property rights over the material you provide to
   the service. All account data remains yours. We do not use this data for any
   other purpose than to provide the services.
1. You may provide us with feedback, suggestions, and ideas about the service.
   You agree that we own all rights to use and incorporate the feedback you
   provide in any way, including in future enhancements and modifications to the
   service, without payment or attribution to you.
1. You must not modify another website so as to falsely imply that it is
   associated with our Services.

## Privacy and security of your data

1. We take many measures to protect and secure your data through backups,
   redundancies, and encryption. You entrust us with your farm data and we take
   that trust to heart.
1. You own all right, title, and interest to your website data. We obtain no
   rights from you to your farm data. We do not collect and analyze personal
   information from web users and we do not use behavioral insights to sell
   advertisements. We will never collect or store any personally identifiable
   information and we will never abuse your visitor’s privacy. We will
   never sell or share your farm data to any third-parties.
1. You agree to comply with all applicable laws including all privacy and data
   protection regulations.

## General conditions

1. Your use of the Services is at your sole risk. We provide these Services on an “as is” and “as available” basis. We do take uptime of our applications seriously. Visit
   [our status page](https://status.brinjel.com) to see the status of our Services.
1. We design our Services with care, based on our own experience and the
   experiences of customers who share their time and feedback. However, there
   is no such thing as a service that pleases everybody. We make no guarantees
   that our Services will meet your specific requirements or expectations.
1. We also test all of our features extensively before shipping them. As with
   any software, our Services inevitably have some bugs. We track the bugs
   reported to us and work through priority ones, especially any related to
   security or privacy. Not all reported bugs will get fixed and we don’t
   guarantee completely error-free Services.
1. Technical support is provided by email. Email responses are provided on the reasonable effort basis without guaranteed response time.
1. We as humans can access your data to help you with support requests you make and to maintain and safeguard Brinjel to ensure the security of your data and the service as a whole.
1. We use third party vendors to provide the necessary hardware, storage, payment processing and related technology required to run the Services. You can see
   [a list of all subprocessors](/en/policies/privacy) here.

## Liability

We mention liability throughout these Terms but to put it all in one section:

**You expressly understand and agree that the Host shall not be liable, in law or in equity, to you or to any third party for any direct, indirect, incidental, lost profits, special, consequential, punitive or exemplary damages, including, but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if the Host has been advised of the possibility of such damages), resulting from:**

1. **use or inability to use the service;**
1. **unauthorized or altered access to data transmission;**
1. **statements or actions of a third party about the service;**
1. **cancelling your account;**
1. **any other service related question.**

The failure of the Host to exercise or enforce any right or provision of the
Terms of Service does not constitute a waiver to that right or
provision. The Terms of Use constitute the entire agreement between you and the
Host and govern your use of the Service, replacing all previous agreements
between you and the Host (including previous versions of the Terms of Use).

This agreement shall be governed by the laws of France, and the courts of
France shall have exclusive jurisdiction to hear and determine all issues that
may arise under or in relation to this agreement.
