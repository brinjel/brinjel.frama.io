---
title: "Privacy Policy"
---

_Last update: 14 April 2024_

Our guiding principle is to collect only what we need and that we will solely
process this information to provide you with the service you signed up for.

We use a select number of trusted external service providers for certain
service offerings. These service providers are carefully selected and meet high
data protection, data privacy and security standards.

We only share information with them that is required for the services offered
and we contractually bind them to keep any information we share with them as
confidential and to process personal data only according to our instructions.

Here’s what that means in practice:

## What we collect, what we use it for and services we use

1. An email address is required to create an account. You need to provide us with your email address if you want to create a Brinjel account. That’s just so you can log in and personalize your new account, and we can send you invoices, updates or other essential information.
1. A persistent first-party cookie is stored to remember you’re logged in. If you log in to your Brinjel account, you give us permission to use cookies so you don’t have to log in on each returning session. This makes it easier for you to use our product. A cookie is a piece of text stored by your browser. You can adjust cookie retention settings in your own browser. Cookies that are already stored may be deleted at any time.
1. All of the data that we collect is kept fully secured, encrypted and hosted on a server in Falkenstein, Germany. The server is owned by Hetzner, a European company. This ensures that all of the site data is being covered by the European Union’s strict laws on data privacy. Your farm data never leaves the EU. See [Hetzner privacy policy](https://www.hetzner.com/legal/privacy-policy) for full details. Moreover, backups are encrypted and stored on Scaleway's servers in France. See [Scaleway privacy policy](https://www.scaleway.com/en/privacy-policy/).
1. All emails are sent using a third-party email provider. Transactional emails are sent using Scaleway. We have disabled both open tracking and link tracking on all emails sent. See the [Scaleway privacy policy](https://www.scaleway.com/en/privacy-policy/) for full details.
1. When you write to us with a question or to ask for help. We keep that correspondence, which includes the email address, so that we have a history of past correspondences to reference if you reach out in the future. We use this data solely in connection with answering the queries we receive.
1. The payment process is handled by a third-party payment provider. If you choose to upgrade to a Brinjel paid plan, the billing information and the payment process is handled by Paddle. See the [Paddle Privacy Policy](https://www.paddle.com/legal/privacy) for full details.
1. We use Bunny (another European-owned provider from Slovenia) for a global CDN and photo storage. This ensures that all you farm data are exclusively processed with servers owned and operated by European companies. See [Bunny privacy and data policy](https://bunny.net/privacy) for full details.
1. If you choose to subscribe to our newsletter, we use Keila to send those news. We have disabled both open tracking and link tracking on all emails sent. Keila is a free software, made and hosted in the EU. See the [Keila Privacy Policy](https://www.keila.io/privacy) for full details.

## Retention of data

1. We will retain your information as long as your account is active, as necessary to provide you with the services or as otherwise set forth in this policy.
1. We will also retain and use this information as necessary for the purposes set out in this policy and to the extent necessary to comply with our legal obligations, resolve disputes, enforce our agreements and protect Brinjel’s legal rights.
1. You can choose to delete your Brinjel account at any time. All your data will be permanently deleted immediately when you delete your account.

## Changes and questions

We may update this policy as needed to comply with relevant regulations and
reflect any new practices. Whenever we make a significant change to our
policies, we will also announce them on our [blog](/en/blog) or social media
profiles.

Contact us at privacy@brinjel.com if you have any questions, comments, or concerns about this privacy policy, your data, or your rights with respect to your information.
