---
title: "Credits"
---

Icons come from the [Heroicons](https://heroicons.com/) and [Remix Icon](https://remixicon.com/) projects.

Illustrations come from the [unDraw](https://undraw.co/) project, by Katerina Limpitsouni.

Brinjel policies are adapted from the [Basecamp open-source policies](https://github.com/basecamp/policies) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) and those of [Framasoft](https://framasoft.org/en/cgu), [Flus](https://app.flus.fr/terms), and [Plausible](https://plausible.io/terms).
