---
title: "Foire aux questions"
---

## Migration de Qrop vers Brinjel

### Pourrai-je facilement migrer depuis Qrop ?

_Oui_, aussi facilement qu'en important votre base de données Qrop actuelle
(`.sqlite` ou `.db`) ! Nous allons également améliorer l'import CSV actuel qui
était bogué. Bien que certaines parties de l'interface utilisateur aient été
modifiées, nous avons essayé de conserver au maximum l'ergonomie de Qrop afin de
faciliter la transition.

### Brinjel sera-t-il toujours un logiciel libre ?

**Oui !** Nous défendons les valeurs du logiciel libre et avons pleinement
confiance en la communauté Qrop. Nous faisons le pari qu'il est possible de
gagner sa vie en faisant du logiciel libre, respectueux des données et à
l'écoute des utilisateurices. La version bureau de Qrop était sous licence
[GPL](https://www.gnu.org/licenses/licenses.html#AGPL). Brinjel quant à lui sera
sont licence [AGPL](https://www.gnu.org/licenses/licenses.html#AGPL), une
licence libre encore plus protectrice puisque comportant _« une clause
additionnelle qui autorise les utilisateurs interagissant avec le logiciel sous
licence via un réseau à recevoir les sources de ce programme »_.

### Pourrai-je continuer à utiliser la version bureau de Qrop ?

Oui, mais aucune maintenance ni correction de bogue ne sera assurée. Tout
notre temps de travail est dorénavant dédié au développement de Brinjel. Le code
restera libre mais [le dépôt Framagit](https://framagit.org/ah/qrop) et les
tickets pertinents seront transférés vers le dépôt de Brinjel.

### Pourrai-je utiliser Brinjel hors-ligne ?

Non, dans un premier temps il ne sera pas possible d'utiliser Brinjel
hors-ligne, à moins de l'installer sur son propre poste. Il nous semble qu'une
bonne partie des utilisateurices de Qrop disposent d'une connexion internet
relativement fiable, et nous préférons développer les fonctionnalités
multipostes. Il n'est pour autant pas exclu de développer un jour une
fonctionnalité hors-ligne.

## Données

### Mes données seront-elles en sécurité ?

Nous ferons des sauvegardes quotidiennes de vos données. Ces sauvegardes seront
chiffrées et stockées dans un autre centre de données. De plus, les données
stockées sur nos serveurs seront compartimentées par ferme. Il sera impossible
à un compte n'ayant pas les droit requis d'accéder aux données d'une autre
ferme. Vous pourrez à tout moment télécharger la totalité des données de votre
ferme stockées sur nos serveurs. Aucune donnée ne sera divulguée ou revendue à
une tierce partie. Vos données vous appartiennent !

## Abonnement

### Serai-je obligé de payer pour utiliser Brinjel ?

Brinjel étant un logiciel libre, toute personne ayant les connaissances
nécessaires pourra l'installer sur son poste et utiliser l'ensemble des
fonctionnalités. En revanche, les personnes souhaitant bénéficier d'un
support pourront payer pour le service
[app.brinjel.com](https://app.brinjel.com). Ce modèle assure un revenu stable
permettant de se consacrer pleinement au développement et à la maintenance de
l'application et de son infrastructure.

### Combien ça va me coûter ?

Conscient des difficultés financières de nombre de collègues, l'abonnement
coûtera _5 € par mois_ ou _55 € par an_. Bien sûr, selon vos possibilités
financières et la taille de votre ferme, il vous sera possible de payer plus !
Aucun contrôle ne sera fait sur ces critères. Si nécessaire, nous pourrons
imposer certaines limites sur la taille de vos données (par exemple, stocker de
nombreuses photos peut avoir un coût). Vous nous faites confiance, nous
vous faisons confiance.

## Formations

### Est-ce que des formations au logiciel sont prévues ?

Oui, des formations à la nouvelle version auront lieu dès l'hiver 2023. Les
formations sont organisées par l'Atelier paysan, en général en
automne-hiver.
[Voir directement sur le site de l'Atelier paysan](https://www.latelierpaysan.org/Formations)
les formations prévues.

### Je n'ai pas trouvé de formations, puis-je en proposer une ?

Oui, si vous pensez pouvoir rassembler une dizaine de personnes. Il faut alors
contacter [la responsable formation](mailto:a.sombardier@latelierpaysan.org?subject=[Qrop]%20Organisation%20d'une%20formation) de l'Atelier
paysan.
