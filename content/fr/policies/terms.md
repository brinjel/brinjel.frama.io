---
title: "Conditions générales d'utilisation"
---

_Dernière modification : 14 avril 2024._

## Définitions

Les « services » désignent les sites web `brinjel.com` et `app.brinjel.com`.

L’« hébergeur », « nous », « notre » et « nos » désignent l’éditeur du site
défini dans les [mentions légales](/fr/policies/legal-notice).

« L'utilisateur» et « vous » désignent toute personne physique ou morale faisant
usage de nos services.

Le terme « C.G.U. » (conditions générales d’utilisation) désigne le présent
document qui défini les règles et les droits qui incombent à la fois à l'hébergeur et
à l'utilisateur.

## Préambule

Nous pouvons être amenés à modifier nos C.G.U. à tout moment. À chaque fois que
nous apporterons des modifications significatives, nous l'annoncerons sur notre
[blog](/fr/blog).

À tout moment, l'utilisation de nos services implique l'acceptation de nos C.G.U.
les plus récentes. Ceci est valable pour l'ensemble de nos services, ainsi que
pour tout nouveau produit ou toute fonctionnalité que nous ajouterons à nos
services.

Si nous n'acceptez pas nos C.G.U., vous ne pouvez pas utiliser nos services.

**Note :** les C.G.U. suivantes ne s'appliquent qu'aux services que nous proposons
et non à la version autohébergée de Brinjel.

## Conditions d'utilisation des comptes

1. Vous êtes responsable de la sécurité de votre compte et de votre mot de
   passe. L'hébergeur ne peut être tenu pour responsable de quelconques pertes
   ou dommages résultant de votre non-respect de cette obligation de sécurité.
1. Vous êtes responsable de tout contenu posté et de l’activité qui se produit
   sous votre compte. Ceci inclut le contenu posté et l’activité de n'importe
   quel utilisateur de votre compte .
1. Vous ne pouvez pas utiliser les services à des fins illégales ou non
   autorisées. Vous ne devez pas transgresser les lois de votre pays en
   utilisant nos services.
1. Les comptes ne peuvent être créés et utilisés que par des humain·es. Les
   comptes créés par les robots ou autres méthodes automatisées pourront être
   supprimés sans avertissement.
1. Vous ne pouvez pas vendre, échanger, revendre, ou exploiter dans un but
   commercial non autorisé un compte du service.

## Conditions de paiement, de remboursement

1. Pour notre essai gratuit, nous expliquons la durée de l'essai lorsque vous
   vous inscrivez. Nous ne vous demandons pas votre carte de crédit et --- tout
   comme pour les clients qui paient pour nos services --- nous ne vendons pas vos
   données. Après la période d'essai, vous devez payer à l'avance pour continuer
   à utiliser le service. Si vous ne payez pas, nous bloquerons votre compte et
   il sera inaccessible jusqu'à ce que vous effectuiez le paiement. Si votre
   compte est gelé depuis 60 jours, nous le mettrons en file d'attente pour une
   résiliation automatique.
1. Si vous passez d'un essai gratuit à une offre payante, nous vous facturerons
   immédiatement et votre de période d'essai gratuit prend fin.
1. Les clients payants sont facturés automatiquement par carte de crédit ou par
   PayPal, selon leur préférence.
1. Nos processus de commande et de paiement sont menés par notre revendeur en ligne et
   vendeur officiel Paddle.com, qui s'occupe également des demandes aux
   services à la clientèle et des retours. Tous les frais incluent les taxes, les
   prélèvements ou les droits imposés par les autorités fiscales. Paddle
   collectera ces taxes au nom de l'autorité fiscale et les remettra aux autorités
   fiscales. Pour plus d'informations, veuillez consulter les
   [conditions d'utilisation de Paddle](https://www.paddle.com/legal/terms).
1. Aucune donnée de paiement (par ex., numéro de carte bancaire) n’est
   manipulée directement par l'hébergeur. Le paiement est considéré comme validé à partir
   du moment où Paddle le fait savoir à l'hébergeur (par ex., à travers un
   message de retour automatique). Si Paddle retourne un message
   indiquant l’échec du paiement, l’utilisateur ne saurait faire valoir ses droits
   auprès de l'hébergeur.
1. Vous pouvez passer à une offre supérieure à tout moment via les paramètres de votre compte. Dans ce cas, vous bénéficierez immédiatement de l'offre supérieure et vous serez facturé au prorata lors de la prochaine facture. Vous pouvez passer d'une offre mensuelle à une offre annuelle égale ou supérieure à tout moment. Dans ce cas, vous serez facturé immédiatement au prorata et un nouveau cycle de paiement débutera le jour du paiement. Pour des raisons techniques, toute autre modification de votre offre (passage à une offre inférieure ou d'un paiement annuel à un paiement mensuel) ne pourra être réalisée automatiquement.
1. Les frais payés en vertu du présent règlement ne sont pas remboursables.

## Résiliation

1. Vous êtes le seul responsable de la bonne résiliation de votre compte. Pour
   résilier votre compte, un lien est disponible dans votre espace personnel,
   sans aucune question de notre part. Un courriel demandant la résiliation de
   votre compte n'est pas automatiquement considéré comme une annulation. Si vous
   avez besoin d'aide pour résilier votre compte, vous pouvez toujours
   [nous contacter](/fr/contact).
1. Si vous résiliez votre abonnement avant la fin de la période de paiement en
   cours (mensuelle ou annuelle), votre résiliation sera effective uniquement à
   la fin de cette période de paiement. Vous ne serez alors plus prélevé.
1. L'hébergeur, à sa seule discrétion, a le droit de suspendre ou de résilier
   votre compte et de refuser toute utilisation actuelle ou future du service.
   Cette résiliation du service entraînera la désactivation de l’accès à votre
   compte, et la restitution de tout le contenu.
1. L'hébergeur se réserve le droit de refuser le service à n’importe qui pour
   n’importe quelle raison à tout moment. Nous avons cette clause pour nous
   prémunir des quelques personnes faisant des choses néfastes sur nos services.
1. L'hébergeur se réserve également le droit de résilier votre compte si votre
   adresse électronique n'a pas été validée au bout d'un mois.
1. Tout comportement abusif (notamment les menaces) à l'encontre d'un·e
   client·e, employé·e ou responsable de notre entreprise
   entraînera la résiliation immédiate de votre compte.

## Modifications des services et des prix

1. Nous nous réservons le droit, à tout moment et de temps à autre, de modifier
   ou d'interrompre, temporairement ou définitivement, toute partie du service,
   avec ou sans préavis.
1. Nous modifions parfois les prix de nos services. Lorsque nous le
   faisons, nous avons tendance à exempter les clients existants de ces
   changements. Toutefois, nous pouvons décider de modifier les prix pour les
   clients existants. Dans ce cas, nous donnons un préavis d'au moins 30 jours
   et nous vous informons par l'intermédiaire de l'adresse électronique que vous
   nous avez communiquée. Nous pouvons également communiquer ces changements
   sur [notre blog](/fr/blog/) ou sur les services concernés eux-mêmes.
1. L'hébergeur ne sera pas responsable envers vous ou envers un tiers de toute
   modification, changement de prix, suspension ou interruption du service.

## Propriété du contenu, droits d'auteur et marques déposées

1. Vous êtes seul responsable du contenu et des autres éléments que vous
   soumettez, publiez, transmettez, envoyez par courriel ou affichez par
   l'intermédiaire nos services.
1. Nous ne revendiquons aucun droit de propriété intellectuelle sur les
   informations que vous fournissez aux services. Toutes les données de votre
   compte vous appartiennent. Nous n'utilisons pas ces données pour autre chose
   que fournir les services.
1. Vous pouvez nous faire part de vos réactions, suggestions et idées concernant
   le service. Vous acceptez que nous possédions tous les droits d'utiliser et
   d'incorporer le retour d'information que vous fournissez de quelque manière
   que ce soit, y compris dans les améliorations et modifications futures des
   services, sans paiement ni attribution à votre égard.
1. Vous ne devez pas modifier un autre site web de manière à suggérer faussement
   qu'il est associé à nos services.

## Confidentialité et sécurité de vos données

1. Nous prenons de nombreuses mesures pour protéger et sécuriser vos données par
   le biais de sauvegardes, de redondances et de chiffrage. Vous nous confiez vos
   données agricoles et nous prenons cette confiance à cœur.
1. Vous possédez tous les droits, titres et intérêts relatifs aux données de
   votre ferme. Nous n'obtenons aucun droit de votre part sur les données de
   votre ferme. Nous ne collectons pas et n'analysons pas les informations
   personnelles des internautes et nous n'utilisons pas les informations
   comportementales pour vendre des publicités. Nous ne vendrons ni ne
   partagerons jamais les données de votre ferme avec des tiers.
1. Vous acceptez de vous conformer à toutes les lois applicables, y compris
   toutes les réglementations relatives à la protection de la vie privée et des
   données.

## Conditions générales

1. Vous utilisez nos services à vos risques et périls. Les services sont fournis
   « tel quel ». Nous prenons au sérieux la disponibilité de notre application.
   Visitez notre [page d'état](https://status.brinjel.com) pour voir l'état de
   nos services ainsi que les éventuelles interventions en cours.
1. Nous concevons nos services avec soin, sur la base de notre propre expérience
   et de celle des clients qui nous font part de leur temps et de leurs
   commentaires. Cependant, aucun service ne peut plaire à tout le monde. Nous
   ne garantissons pas que nos services répondront à vos exigences ou à vos
   attentes spécifiques.
1. Nous testons également toutes nos fonctionnalités de manière approfondie
   avant de les livrer. Comme tout logiciel, nos services comportent
   inévitablement des bogues. Nous suivons les bogues qui nous sont signalés et
   travaillons sur ceux qui sont prioritaires, en particulier ceux qui sont liés
   à la sécurité ou à la vie privée. Tout bogue signalé ne sera pas
   systématiquement corrigé et nous ne garantissons pas des services totalement
   exempts d'erreurs.
1. L'assistance technique est fournie par courriel. Les réponses
   aux courriels sont fournies sur la base d'un effort raisonnable sans garantie
   de temps de réponse.
1. En tant qu'êtres humains, nous pouvons accéder à vos données pour vous aider à répondre à vos demandes d'assistance et pour maintenir et protéger Brinjel afin de garantir la sécurité de vos données et du service dans son ensemble.
1. Nous utilisons des fournisseurs tiers pour fournir le matériel nécessaire, le
   stockage, le traitement des paiements et la technologie connexe nécessaire
   pour faire fonctionner les services. Vous pouvez consulter la liste de tous
   [les sous-traitants ici](/fr/policies/privacy).

## Responsabilité

Nous parlons de responsabilité tout au long de ces conditions, mais nous
souhaitons la regrouper dans une seule section :

**Vous comprenez et acceptez que l'hébergeur ne puisse être tenu responsable des dommages directs, indirects ou fortuits, notamment les dommages pour perte de profits, de clientèle, d’accès, de données ou d’autres pertes intangibles (même si l'hébergeur est informé de la possibilité de tels dommages) et qui résulteraient de :**

1. **l’utilisation ou de l’impossibilité d’utiliser les services ;**
1. **l’accès non autorisé ou altéré de la transmission des données ;**
1. **les déclarations ou les agissements d’un tiers sur les services ;**
1. **la résiliation de votre compte ;**
1. **toute autre question relative au service.**

L’échec de l'hébergeur à exercer ou à appliquer tout droit ou disposition des
C.G.U. ne constitue pas une renonciation à ce droit ou à cette disposition. Les
C.G.U. constituent l’intégralité de l’accord entre vous et
l'hébergeur et régissent votre utilisation du service, remplaçant tous les
accords antérieurs entre vous et l'hébergeur (y compris les versions précédentes
des C.G.U.).

Le présent accord est régi par les lois de France, et les tribunaux de France
ont compétence exclusive pour entendre et trancher toutes les questions
susceptibles de se poser dans le cadre du présent accord ou en relation avec
celui-ci.
