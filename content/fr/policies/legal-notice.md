---
title: "Mentions légales"
---

## Éditeur

E.I John Hoarau\
36 chemin Désiré\
97429 Petite-Île\
France

N° Siret : 880 817 937 00027

[Contact](/fr/contact)

## Hébergeur

Hetzner Online GmbH\
Industriestr. 25\
91710 Gunzenhausen\
Allemagne

Téléphone : +49 (0)9831 505-0
