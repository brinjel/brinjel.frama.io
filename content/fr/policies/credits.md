---
title: "Crédits"
---

Les icônes sont issues du projet [Heroicons](https://heroicons.com/).

Les illustrations proviennent du projet [unDraw](https://undraw.co/), par Katerina Limpitsouni.

Les C.G.U. et la politique de confidentialité sont librement inspirées de celles de [Framasoft](https://framasoft.org/fr/cgu), [Flus](https://app.flus.fr/terms), [Plausible](https://plausible.io/terms) et [Basecamp](https://basecamp.com/about/policies).
