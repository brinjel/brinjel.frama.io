---
title: "Politique de confidentialité"
---

_Dernière mise à jour : 14 avril 2024_

Notre principe directeur est de ne collecter que ce dont nous avons besoin et de
ne traiter ces informations que pour vous fournir le service auquel vous avez
souscrit.

Nous faisons appel à un certain nombre de prestataires de services externes de
confiance pour certaines offres de services. Ces prestataires sont soigneusement
sélectionnés et répondent à des normes élevées en matière de protection des
données, de confidentialité et de sécurité.

Nous ne partageons avec eux que les informations nécessaires aux services
proposés et nous nous engageons contractuellement à garder confidentielles toutes
les informations que nous partageons avec eux et à ne traiter les données à
caractère personnel que conformément à nos instructions.

Voici ce que cela signifie en pratique :

## Ce que nous collectons, l'usage que nous en faisons et les services que nous utilisons

1. Une adresse email est nécessaire pour créer un compte. Vous devez nous
   fournir votre adresse email si vous souhaitez créer un compte Brinjel. Ceci
   afin que vous puissiez vous connecter et personnaliser votre nouveau compte,
   et que nous puissions vous envoyer des factures, des mises à jour ou d'autres
   informations essentielles.
1. Un cookie persistant est stocké pour se souvenir que vous êtes connecté. Si
   vous vous connectez à votre compte Brinjel, vous nous autorisez à utiliser
   des cookies afin que vous n'ayez pas à vous connecter à chaque nouvelle
   session. Cela vous permet d'utiliser plus facilement notre produit. Un cookie
   est un morceau de texte stocké par votre navigateur. Vous pouvez régler les
   paramètres de conservation des cookies dans votre propre navigateur. Les
   cookies déjà stockés peuvent être supprimés à tout moment.
1. Toutes les données que nous recueillons sont entièrement sécurisées et chiffrées à
   Falkenstein, en Allemagne. Le serveur appartient à Hetzner, une société
   européenne. Cela garantit que toutes les données du site sont couvertes par
   les lois strictes de l'Union européenne sur la confidentialité des données.
   Les données de votre ferme ne quittent jamais l'Union européenne. Voir la [politique de confidentialité de Hetzner](https://www.hetzner.com/legal/privacy-policy) pour plus de détails.
   De plus, les sauvegardes sont chiffrées et stockées chez Scaleway, un fournisseur français.
   Voir la [politique de confidentialité de Scaleway](https://www.scaleway.com/fr/politique-confidentialite/)
1. Tous les courriels sont envoyés par l'intermédiaire d'un fournisseur de
   services de messagerie tiers. Les courriels transactionnels sont envoyés par
   Scaleway. Il n'y a aucun suivi des ouvertures et des liens, pour aucun
   courriel. Voir la [politique de confidentialité de Scaleway](https://www.scaleway.com/fr/politique-confidentialite/).
1. Lorsque vous nous écrivez pour poser une question ou demander de l'aide, nous
   conservons cette correspondance, y compris l'adresse électronique, afin de
   disposer d'un historique des correspondances passées auquel nous pourrons nous
   référer si vous nous contactez à l'avenir. Nous utilisons ces données
   uniquement pour répondre aux questions que nous recevons.
1. Le processus de paiement est géré par un fournisseur de paiement tiers. Si vous choisissez de passer une offre payante de Brinjel, les informations de paiement et le processus de paiement sont gérés par Paddle. Voir la [politique de confidentialité de Paddle](https://www.paddle.com/legal/privacy) pour plus de détails.
1. Nous utilisons Bunny (un fournisseur européen basé en Slovénie) pour les services CDN et de stockage de photos. Cela garantit que toutes les données de votre ferme sont exclusivement traitées par des serveurs d'entreprises européennes. Voir la [politique de confidentialité de Bunny](https://bunny.net/privacy) pour plus de détails.
1. Si vous faites le choix de vous abonner à notre newsletter, nous utilisons Keila pour vous envoyer des nouvelles. Nous avons désactivé le traçage d'ouverture et des liens de tous les courriels que nous envoyons. Keila est un logiciel libre conçu et hébergé en Europe. Voir la [politique de confidentialité de Keila](https://www.keila.io/privacy) pour plus de détails.

## Conservation des données

1. Nous conserverons vos informations aussi longtemps que votre compte sera
   actif, dans la mesure où cela est nécessaire pour vous fournir les services
   ou comme indiqué dans la présente politique.
1. Nous conserverons et utiliserons également ces informations si nécessaire aux
   fins énoncées dans la présente politique et dans la mesure nécessaire pour
   nous conformer à nos obligations légales, résoudre les litiges, appliquer nos
   accords et protéger les droits légaux de Brinjel.
1. Vous pouvez choisir de supprimer votre compte Brinjel à tout
   moment. Toutes vos données seront définitivement supprimées dès que vous
   supprimerez votre compte.

## Modifications et questions

Nous pouvons mettre à jour cette politique si nécessaire afin de nous conformer
aux réglementations pertinentes et de refléter les nouvelles pratiques. Chaque
fois que nous apporterons une modification importante à nos politiques, nous
l'annoncerons également sur [le blog](/fr/blog) de Brinjel ou sur nos profils
de médias sociaux.

Contactez-nous à l'adresse privacy@brinjel.com si vous avez des questions, des
commentaires ou des inquiétudes concernant cette politique de confidentialité,
vos données ou vos droits relatifs à vos informations.
