---
date: 2024-08-25
title: "Utilisez Brinjel à plusieurs"
slug: "2024-08-25-utilisez-brinjel-a-plusieurs"
summary: Il est désormais possible de créer plusieurs comptes par ferme, avec différents rôles et permissions.
tags: ["Nouveautés"]
---


## Accès aux fermes

Lorsque que vous vous connectez à votre compte Brinjel, vous voyez désormais
une liste des fermes auxquelles vous avez accès. Si vous avez des invitations
pour d'autres fermes, vous les verrez également apparaitre.

![Listes des fermes et des invitations](/img/farm-list-fr.png)

En cliquant sur le bouton **Accepter**, la ferme devient accessible :

![Listes des fermes avec invitation acceptée](/img/farm-list-invitation-accepted-fr.png)

Il suffit alors de cliquer sur la ferme à laquelle vous voulez accéder et le
plan de culture apparaitra.

Les adresses des pages contiennent désormais le nom de la ferme. Il
est donc possible de consulter plusieurs fermes en même temps en ouvrant plusieurs
onglets ou fenêtres de votre navigateur.

![Consultation simultanée de plusieurs fermes](/img/multiple-farms-fr.png)

## Inviter des personnes sur sa ferme

Pour inviter une personne sur votre ferme, il faut d'abord [souscrire à un abonnement](https://docs.brinjel.com/fr/trial-to-paid/). Ensuite, il faut se rendre dans **Paramètres** puis **Membres de l'équipe**. En cliquant sur le bouton « Ajouter invitation » un formulaire apparait. Il faut alors saisir l'adresse email de la personne et le rôle que vous souhaitez lui attribuer.

![Invitation d'une personne](/img/inviting-member-fr.png)

La personne invitée recevra alors un email d'invitation. Si elle possède déjà
un compte Brinjel, il lui suffira de se connecter à son compte et d'accepter
l'invitation depuis [la liste des fermes](https://app.brinjel.com/farms). Dans le cas contraire, elle pourra
créer un compte grâce au lien contenu dans l'email d'invitation.
