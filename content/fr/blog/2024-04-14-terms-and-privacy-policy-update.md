---
date: 2024-04-14
title: "Modification des C.G.U"
slug: "2024-04-14-modification-des-cgu-et-de-la-politique-de-confidentialite"
tags: "Entreprise"
summary: Modification mineure des C.G.U. et de la politique de confidentialité.
---

Dans le cadre de la mise en place du système de facturation, nous avons été amenés à modifier nos [C.G.U.](/fr/policies/terms) et [notre politique de confidentialité](/fr/policies/privacy). Afin notamment de préciser les modalités de paiement et de changement d'offres.
