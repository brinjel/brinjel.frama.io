---
date: 2025-01-21
title: "Formation de l’Atelier paysan"
summary: Il est désormais possible de créer plusieurs comptes par ferme, avec différents rôles et permissions.
summary: Une formation à distance à l'utilisation de Brinjel est proposée le 21 et 28 février 2025 par l'Atelier paysan.
tags: ["Formations"]
---

Les informations concernant la formation et les inscriptions sont disponibles sur le [site de l'Atelier paysan](<https://latelierpaysan.org/Formation-a-l'utilisation-du-logiciel-BRINJEL-(anciennement-QROP)-de-planification-des-cultures-en-maraichage-6339>).
