---
date: 2024-06-10
title: "Migration du serveur de Brinjel"
slug: "2024-06-10-migration-serveur-de-brinjel"
tags: "Maintenance"
summary: Une maintenance est prévue ce **mardi 11 juin** à partir de 10 h afin de migrer Brinjel vers un nouveau serveur.
---

La coupure ne devrait pas durer plus de 2 heures. Pour plus d'informations,
vous pouvez consulter [l'état de services](https://status.brinjel.com).

_La maintenance est désormais terminée._
