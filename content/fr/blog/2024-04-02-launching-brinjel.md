---
date: 2024-04-02
title: "Lancement de Brinjel"
slug: "lancement-de-brinjel"
summary: Après plus d'un an de développement, je suis heureux de vous annoncer la sortie de Brinjel, le logiciel libre de planification et de suivi des cultures en maraichage.
tags: ["Nouveautés"]
---

Brinjel est le successeur de [Qrop](https://qrop.frama.io/) et reprend ses
fonctionnalités, tout en étant accessible en ligne. Si vous utilisiez déjà
Qrop, vous pourrez importer la totalité de vos données dans Brinjel, en
quelques clics. Dans les semaines à venir, de nouvelles fonctionnalités seront
ajoutées, qui faciliteront notamment le travail à plusieurs. Je publierai
bientôt une documentation détaillée et des tutoriels.

Vous pouvez [me contacter](mailto:support@brinjel.com) pour proposer des
fonctionnalités ou m'informer des problèmes ou bugs que vous rencontreriez.

Merci pour votre intérêt pour un outil de planification libre, créé par un
ancien maraicher à l'écoute de ses collègues.

![Plan de culture dans Brinjel](/img/brinjel-release-fr.png)

Venez le tester gratuitement pendant un mois !

[Essayer Brinjel gratuitement](https://app.brinjel.com/register)

