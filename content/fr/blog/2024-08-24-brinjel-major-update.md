---
date: 2024-08-24
title: "Mise à jour importante de Brinjel"
slug: "2024-08-24-mise-a-jour-importante-de-brinjel"
tags: "Maintenance"
summary: Une maintenance est prévue **dimanche 25 août 2024** à partir de 8 h (UTC+2) afin de réaliser une mise à jour importante de Brinjel.
---

Cette mise à jour apportera une amélioration des performances et la possibilité
d'avoir plusieurs utilisateurs et utilisatrices par ferme.

La coupure ne devrait pas durer plus de 2 heures. Pour plus d'informations, vous pouvez consulter [l'état de services](https://status.brinjel.com).

_La maintenance est désormais terminée._
