---
title: "À propos"
description: "La planification libre pour le maraichage"
slug: "a-propos"
---

Bonjour !

Je suis André Hoarau, le créateur de Brinjel et de [Qrop](https://qrop.frama.io).

Après avoir suivi une formation en maraichage à Florac (France), j'ai intégré
un espace-test agricole dans l'Aude en 2016. Pendant ces deux années, j'ai
produit des légumes bio sur planches permanentes. Très inspiré par les
maraichers Eliot Coleman et Jean-Martin Fortier, j'ai toujours accordé beaucoup
d'importance à l'organisation et à la planification du travail. Ayant une
formation en informatique, j'ai naturellement eu l'idée de créer une
application dédiée à la planification et au suivi des cultures en maraichage.

C'est la rencontre avec l'[Atelier paysan](https://latelierpaysan.org), et
notamment la création d'un groupe de travail dédié en son sein, qui a permis la
concrétisation de ce projet. C'est ainsi qu'est né
[Qrop](https://qrop.frama.io), dont le développement a commencé en 2018 alors
que j'étais salarié agricole et à la recherche de terres pour mon installation.

Qrop a connu un beau développement, notamment grâce la communauté qui s'est
formée autour, mais le développement a ralenti à partir de 2020, date de mon
installation en tant que maraicher sur une ferme bio de trois hectares en
planches permanentes.

En 2022, j'ai été contraint d'arrêter mon activité de maraichage, et j'ai
décidé de me reconvertir en tant que développeur web. Ayant toujours la passion
du maraichage, et souhaitant continuer la belle aventure de Qrop, j'ai décidé
de créer Brinjel, son successeur.
