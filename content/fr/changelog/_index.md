---
title: Quoi de neuf ?
subtitle: Nous améliorons constamment Brinjel.
description: Voici quelques unes des fonctionnalités et améliorations que nous avons apportées à Brinjel depuis son lancement.
url: quoi-de-neuf
_build:
  render: true
cascade:
  _build:
    render: false
    list: true
---
