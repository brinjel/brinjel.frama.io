---
title: Meilleur filtrage des séries dans l'assolement
date: 2024-12-03
tags: Nouveauté
---

![Filtrage des séries dans l'assolement](/img/changelog-field-map-new-planting-filters-fr.png)

Dans l'assolement, il est désormais possible de filtrer les séries non assolée selon leur type (semées ou plantées) et leurs étiquettes. En sélectionnant plusieurs étiquettes, seules les séries possédant _toutes_ les étiquettes seront affichées.
