/** @type {import('stylelint').Config} */
export default {
    rules: {
        "block-no-empty": true,
        "color-no-invalid-hex": true,
        "no-invalid-double-slash-comments": true
    }
};
